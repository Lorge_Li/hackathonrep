package com.example.financemanager.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Inheritance
@DiscriminatorColumn(name = "account_type")
@Table(name = "bank_account")
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    private Integer accountId;

    @Column(name = "account_bal")
    private Double balance;

    @Column(name = "account_type")
    private String accountType; // Type: Checking, Saving

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
