package com.example.financemanager.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@DiscriminatorValue("Saving")
public class SavingAccount extends BankAccount{
    private Double annualInterestRate;

}
