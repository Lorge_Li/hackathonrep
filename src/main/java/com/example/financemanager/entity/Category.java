package com.example.financemanager.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Integer categoryId;

    @Column(name = "category_name")
    private String name;

    @Column(name = "is_default")
    private Boolean isDefault; // Default categories: Utilities, Travel, Groceries, Drinks, Rent, Cell Phone, Car Payments, Fun Money.

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
