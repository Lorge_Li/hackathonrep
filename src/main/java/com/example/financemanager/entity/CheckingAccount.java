package com.example.financemanager.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@DiscriminatorValue("Checking")
public class CheckingAccount extends BankAccount{
    private Double insufficientFundsFee;
}
